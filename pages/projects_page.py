from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class ProjectsPage:
    def __init__(self, browser):
        self.browser = browser

    def wait_for_load(self):
        wait = WebDriverWait(self.browser, 10)
        selector = (By.CSS_SELECTOR, '#j_searchButton')
        return wait.until(expected_conditions.element_to_be_clickable(selector))

    def search(self, query):
        self.browser.find_element(By.CSS_SELECTOR, '#search').send_keys(query)
        self.browser.find_element(By.CSS_SELECTOR, '#j_searchButton').click()

    def get_project_from_project_list(self, project_name):
         return self.browser.find_element(By.TAG_NAME, 'td').find_element(By.LINK_TEXT, project_name)
