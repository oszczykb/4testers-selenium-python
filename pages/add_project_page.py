from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class AddProjectPage:
    def __init__(self, browser):
        self.browser = browser

    def wait_for_load(self):
        wait = WebDriverWait(self.browser, 10)
        selector = (By.CSS_SELECTOR, '#save')
        return wait.until(expected_conditions.element_to_be_clickable(selector))

    def add_project(self, project_name, project_prefix):
        name_input = self.browser.find_element(By.CSS_SELECTOR, "#name")
        name_input.send_keys(project_name)

        prefix_input = self.browser.find_element(By.CSS_SELECTOR, "#prefix")
        prefix_input.send_keys(project_prefix)

        self.browser.find_element(By.CSS_SELECTOR, "#save").click()
