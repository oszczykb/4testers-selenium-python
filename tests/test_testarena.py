import string
import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
import rstr

from pages.administration_page import AdministrationPage
from pages.cockpit_page import CockpitPage
from pages.login_page import LoginPage
from pages.add_project_page import AddProjectPage
from pages.project_added_page import ProjectAddedPage
from pages.projects_page import ProjectsPage


#
# Rozwiązanie zadania znajduje się w metodzie test_added_project_can_be_found_by_name
#

@pytest.fixture()
def browser():
    browser = Chrome(executable_path=ChromeDriverManager().install())

    login_page = LoginPage(browser)
    login_page.load()
    login_page.login('administrator@testarena.pl', 'sumXQQ72$L')

    yield browser
    browser.quit()


def test_logout_correctly_displayed(browser):
    assert browser.find_element(By.CSS_SELECTOR, '[title=Wyloguj]').is_displayed() is True


def test_added_project_can_be_found_by_name(browser):
    # dane projektu
    project_name = rstr.rstr(string.ascii_letters, 5)
    project_prefix = "BO" + rstr.rstr(string.ascii_letters, 4)

    # przejdź do administracji, sprawdź czy się wyświetliła
    cockpit_page = CockpitPage(browser)
    cockpit_page.click_administration()

    administration_page = AdministrationPage(browser)
    administration_page_search_button = administration_page.wait_for_load()
    assert administration_page_search_button.is_displayed()

    # przejdź do formularza dodawania projektu, sprawdź czy się wyświetla
    administration_page.click_add_project()

    add_project_page = AddProjectPage(browser)
    add_project_save_button = add_project_page.wait_for_load()
    assert add_project_save_button.is_displayed()

    # wypełnij formularz wygenerowanymi danymi i wyślij, sprawdź czy otwiera się strona szczegółów dodanego projektu
    add_project_page.add_project(project_name, project_prefix)

    project_added_page = ProjectAddedPage(browser)
    project_title = project_added_page.wait_for_load()
    assert project_title.is_displayed()

    # przejdź do projektów
    project_added_page.click_projects()

    projects_page = ProjectsPage(browser)
    projects_search_button = projects_page.wait_for_load()
    assert projects_search_button.is_displayed()

    # wyszukaj dodany projekt po nazwie projektu
    projects_page.search(project_name)

    # pierwszy wiersz listy powinien zawierać nazwę projektu
    assert projects_page.get_project_from_project_list(project_name) is not None
