import time
import pytest
from selenium.webdriver import Chrome
from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import expected_conditions as EC

@pytest.fixture()
def browser():
    browser = Chrome(executable_path=ChromeDriverManager().install())
    browser.get.('http://timvroom.com/selenium/playground/')
    yield browser
    browser.quit()

def test_waiting(browser):
    browser.find_elements(By.LINK_TEXT, 'click then wait').click()
    wait = WebDriverWait(browser, 10)
    link_that_appers = (By.LINK_TEXT, 'click after wait')
    link = wait.until(EC.presence_of_element_located(By.LINK_TEXT, 'click after wait'))
    assert browser.find_element(By.LINK_TEXT, 'click after wait').is_displayed()


def test_waiting(browser):
    browser.execute_script("window.scrollTo(0, 600)")
    assert browser.find_element(By.CSS_SELECTOR, "[name='Ramka produktowa 1 - Wysyłka 0 zł] .swiper-slide-active").is_displayed()
    time.sleep(5)
